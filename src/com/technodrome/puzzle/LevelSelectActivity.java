package com.technodrome.puzzle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.technodrome.puzzle.util.DebugLog;

public class LevelSelectActivity extends Activity {
	private String mDifficulty;
	private boolean isVolumeOn;
	private Button btnSounds;
	private Button btnLevelStart;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_level_select);
		
		btnSounds = (Button) findViewById(R.id.btn_level_settings);
		btnLevelStart = (Button) findViewById(R.id.btn_level_start);
		mDifficulty = getIntent().getStringExtra("difficulty");
		isVolumeOn = getIntent().getBooleanExtra("volume", true);

		if (!isVolumeOn) {
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
		} else {
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
		}
		btnLevelStart.setEnabled(false);
		btnLevelStart.setBackgroundResource(R.drawable.btn_start_select);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SoundManager.resume();
	}

	@Override
	protected void onPause() {
		SoundManager.pause();
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent(this,MainActivity.class);
		startActivity(i);
		finish();
	}
	
	public void easyClick(View view) {
		mDifficulty = "easy";
		if(DebugLog.DEBUG_ON)
			Toast.makeText(this, mDifficulty, Toast.LENGTH_SHORT).show();
		btnLevelStart.setEnabled(true);
		btnLevelStart.setBackgroundResource(R.drawable.btn_start_style);
	}
	
	public void mediumClick(View view) {
		mDifficulty = "medium";
		if(DebugLog.DEBUG_ON)
			Toast.makeText(this, mDifficulty, Toast.LENGTH_SHORT).show();
		btnLevelStart.setEnabled(true);
		btnLevelStart.setBackgroundResource(R.drawable.btn_start_style);
	}
	
	public void hardClick(View view) {
		mDifficulty = "hard";
		if(DebugLog.DEBUG_ON)
			Toast.makeText(this, mDifficulty, Toast.LENGTH_SHORT).show();
		btnLevelStart.setEnabled(true);
		btnLevelStart.setBackgroundResource(R.drawable.btn_start_style);
	}
	
	public void settingsClick(View view) {
		if (isVolumeOn) {
			isVolumeOn = false;
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
			SoundManager.BACKGROUND_MUSIC.setVolume(0.0f, 0.0f);
		} else {
			isVolumeOn = true;
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
			SoundManager.BACKGROUND_MUSIC.setVolume(1.0f, 1.0f);
		}

	}
	
	public void startClick(View view) {
		Intent i = new Intent(this, SelectPuzzleActivity.class);
		i.putExtra("difficulty", mDifficulty);
		i.putExtra("volume", isVolumeOn);
		startActivity(i);
		finish();
	}
	
}
