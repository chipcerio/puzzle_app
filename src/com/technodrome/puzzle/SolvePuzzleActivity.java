package com.technodrome.puzzle;

import java.io.File;

import org.worldsproject.puzzle.PuzzleView;
import org.worldsproject.puzzle.enums.Difficulty;

import com.technodrome.puzzle.util.DebugLog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SolvePuzzleActivity extends Activity {
	private int puzzle;
	private Bitmap image;
	private String difficulty;
	private Difficulty x;
	private PuzzleView pv;
	private TextView mLevel;
	private LinearLayout mPreview;

	@Override
	protected void onStart() {
		super.onStart();
		puzzle = getIntent().getIntExtra("image", 0);
		image = (Bitmap) BitmapFactory.decodeResource(getResources(), puzzle);
		
		difficulty = getIntent().getStringExtra("difficulty");
		
		mLevel = (TextView) findViewById(R.id.txtLevel);
		mLevel.setTextColor(Color.RED);
		mLevel.setText(difficulty.toUpperCase().trim());
		if (DebugLog.DEBUG_ON)
			Toast.makeText(getApplicationContext(), difficulty.toUpperCase().trim(), 
					Toast.LENGTH_SHORT).show();
		
		if (difficulty.equals("easy"))
			x = Difficulty.EASY;
		else if (difficulty.equals("medium"))
			x = Difficulty.MEDIUM;
		else
			x = Difficulty.HARD;
		
		pv = (PuzzleView) findViewById(R.id.puzzleView);
		
		// Now we need to test if it already exists.
		File testExistance = new File(path(puzzle, x.toString()));
		
		if (testExistance != null && testExistance.exists())
			pv.loadPuzzle(path(puzzle, x.toString()));
		else
			pv.loadPuzzle(image, x, path(puzzle, x.toString()));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_puzzle_solve);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		pv.savePuzzle(path(puzzle, x.toString()));
		SoundManager.pause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SoundManager.resume();
	}

	@Override
	protected void onSaveInstanceState(Bundle b) {
		super.onSaveInstanceState(b);
		b.putInt("puzzle", puzzle);
		b.putString("difficulty", x.toString());
	}

	@Override
	protected void onRestoreInstanceState(Bundle b) {
		super.onRestoreInstanceState(b);
		puzzle = b.getInt("puzzle");
		x = Difficulty.getEnumFromString(b.getString("difficulty"));
		pv.loadPuzzle(path(puzzle, x.toString()));
	}

	private String path(int puzzle, String difficulty) {
		String rv = null;
		if (Environment.getExternalStorageState().equals("mounted")) {
			rv = getExternalCacheDir().getAbsolutePath() + "/" + puzzle + "/"
					+ difficulty + "/";
		} else {
			rv = getCacheDir().getAbsolutePath() + "/" + puzzle + "/"
					+ difficulty + "/";
		}
		return rv;
	}
	
	public void onClickPreview(View view) {
		Dialog dialog = new Dialog(this);		
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.d_preview);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setCancelable(true);
		mPreview = (LinearLayout) dialog.findViewById(R.id.linear1);
		mPreview.setBackgroundResource(puzzle);
		dialog.show();
	}
	
	public void playAgainClick(View view) {
		pv.shuffle();
	}
	
	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Puzzle Application")
				.setMessage("Are you sure you want to exit?")
				.setNegativeButton(android.R.string.no, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}

				})
				.setPositiveButton(android.R.string.yes, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						SolvePuzzleActivity.super.onBackPressed();
					}
				}).create().show();
	}

}
