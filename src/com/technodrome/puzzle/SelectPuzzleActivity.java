package com.technodrome.puzzle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.technodrome.puzzle.util.DebugLog;

public class SelectPuzzleActivity extends Activity {
	private int mSelectPuzzle;
	private String mDifficulty;
	private boolean isVolumeOn;
	private Button btnSounds;
	private Button btnLevelStart;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_puzzle_select);
		mDifficulty = getIntent().getStringExtra("difficulty");
		isVolumeOn = getIntent().getBooleanExtra("volume", true);
		btnSounds = (Button) findViewById(R.id.btn_select_settings);
		btnLevelStart = (Button) findViewById(R.id.btn_select_letsgo);
		
		btnLevelStart.setEnabled(false);
		btnLevelStart.setBackgroundResource(R.drawable.btn_letsgo_select);
		
		if (!isVolumeOn)
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
		else
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
		
		
		// init gridview
		GridView gridview = (GridView) findViewById(R.id.grid_puzzle_images);
		
		// gridview set image adapter
		gridview.setAdapter(new ImageAdapter(this));
		
		// gridview set listener
		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				mSelectPuzzle = mImages[position];
				btnLevelStart.setEnabled(true);
				btnLevelStart.setBackgroundResource(R.drawable.btn_letsgo_style);
				if (DebugLog.DEBUG_ON)
					Toast.makeText(SelectPuzzleActivity.this, "id="+mSelectPuzzle, Toast.LENGTH_SHORT)
					.show();
			}
		});
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SoundManager.resume();
	}

	@Override
	protected void onPause() {
		SoundManager.pause();
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent(this,LevelSelectActivity.class);
		i.putExtra("difficulty", mDifficulty);
		startActivity(i);
		finish();
	}
	
	public void settingsClick(View view) {
		if (isVolumeOn) {
			isVolumeOn = false;
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
			SoundManager.BACKGROUND_MUSIC.setVolume(0.0f, 0.0f);
		} else {
			isVolumeOn = true;
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
			SoundManager.BACKGROUND_MUSIC.setVolume(1.0f, 1.0f);
		}

	}
	
	public void letsGoClick(View view) {
		Intent i = new Intent(this, SolvePuzzleActivity.class);
		i.putExtra("difficulty", mDifficulty);
		i.putExtra("image", mSelectPuzzle);
		startActivity(i);
		finish();
	}
	
	public class ImageAdapter extends BaseAdapter {
		private Context mContext;
		
		public ImageAdapter(Context context) {
			mContext = context;
		}

		@Override
		public int getCount() {
			return mImages.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView image;
			if (convertView == null) {
				image = new ImageView(mContext);
				image.setLayoutParams(new GridView.LayoutParams(250, 225));
				image.setScaleType(ImageView.ScaleType.FIT_CENTER);
				image.setPadding(8, 8, 8, 8); 	
				image.setAdjustViewBounds(true);
			} else {
				image = (ImageView) convertView;
			}
			
			image.setImageResource(mImages[position]);
			
			return image;
		}
		
	}
	
	private Integer[] mImages = {
			R.drawable.puzzle_01,
			R.drawable.puzzle_02,
			R.drawable.puzzle_03,
			R.drawable.puzzle_04,
			R.drawable.puzzle_05,
			R.drawable.puzzle_06
	};

}