package com.technodrome.puzzle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	protected boolean isVolumeOn = true;
	private Button btnSounds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_main);
		
		if(isVolumeOn){
			SoundManager.play(this);
		}
		else {
			SoundManager.stop();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SoundManager.resume();
	}

	@Override
	protected void onPause() {
		SoundManager.pause();
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
	
	public void playClick(View view) {
		Intent i = new Intent(this, LevelSelectActivity.class);
		i.putExtra("volume", isVolumeOn);
		startActivity(i);
		finish();
	}
	
	public void settingsClick(View view) {
		if (isVolumeOn) {
			isVolumeOn = false;
			btnSounds.setBackgroundResource(R.drawable.sound_mute);
			SoundManager.BACKGROUND_MUSIC.setVolume(0.0f, 0.0f);
		} else {
			isVolumeOn = true;
			btnSounds.setBackgroundResource(R.drawable.sounds_style);
			SoundManager.BACKGROUND_MUSIC.setVolume(1.0f, 1.0f);
		}
	}

}
